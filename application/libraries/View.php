<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
 /**
  * View Class
  *
  * @author Alexander Makarov
  * @copyright 2008
  * @version 0.9
  * @uses PHP5
  * @link http://rmcreative.ru/
  */
 class View {
     private $layoutVars = array();
 
     private $vars = array();
     private $layout = 'layout';
     private $title = 'Don\'t forget to set title!';
 
     function setLayout($template){
         $this->layout = $template;
     }
 
     function setTitle($title){
         $this->title = $title;
     }
 
     function set($varName, $value){
         $this->vars[$varName] = $value;
     }
 
     function setGlobal($varName, $value){
         $this->layoutVars[$varName] = $value;
     }
 
     /**
      * Fetch template and return it.
      *
      * @param String $template
      */
     function fetch($template){
         /* @var CI CI_Base */
         $CI = &get_instance();
 
         $content = $CI->load->view($template, $this->vars, true);
 
         $this->layoutVars['content'] = $content;
         $this->layoutVars['title'] = $this->title;
 
         return $CI->load->view($this->layout, $this->layoutVars, true);
     }

     function fetchPartial($template){
         /* @var CI CI_Base */
         $CI = &get_instance();
 
         return $CI->load->view($template, $this->vars, true);
     }

     function renderPartial($template){
         echo $this->fetchPartial($template);
     }
 
     /**
      * Renders template to $content.
      *
      * @param String $template
      */
     function render($template){
               echo $this->fetch($template);
     }
 }