<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Layout2
{
    
    var $obj;
    var $layout;
    var $title;
    var $nav;
    var $quantity;
    var $meta_descr = 'Каталог организаций г.Белорецка';
    var $meta_keywords = 'белорецк, организации';
    
    function Layout2($layout = "layout")
    {
        $this->obj =& get_instance();
        $this->layout = $layout;
    }

    function setLayout($layout)
    {
        $this->layout = $layout;
    }

    function setTitle($title)
    {
        $sep = ' / ';
        $start = 'Белорецк';

        $this->title = $title . $sep . $start;
    }

    function setNav($nav)
    {
        $this->nav = $nav;
    }

    function setKeywords($keywords)
    {
        $this->meta_keywords = $keywords;
    }

    function setDescr($descr)
    {
        $this->meta_descr = $descr;
    }

    function setCount($quantity)
    {
        $this->quantity = $quantity;
    }
    
    function view($view, $data=null, $return=false)
    {
        $loadedData = array();

        $loadedData['content'] = $this->obj->load->view($view,$data,true);
        $loadedData['title'] = $this->title;
        $loadedData['nav'] = $this->nav;
        $loadedData['quantity_companies'] = $this->quantity;
        $loadedData['meta_descr'] = $this->meta_descr;
        $loadedData['meta_keywords'] = $this->meta_keywords;
        
        if($return)
        {
            $output = $this->obj->load->view($this->layout, $loadedData, true);
            return $output;
        }
        else
        {
            $this->obj->load->view($this->layout, $loadedData, false);
        }
    }
}