<div class="event">
	<h2 class="time-place"><?php echo mdate("%d %M %Y в %H:%i", $event['date']) ?>, <?php echo $event['place'] ?></h2>
	<ul class="links">
		<?php foreach ($links as $link): ?>
			<li><a href="/event/<?php echo $event['url'] ?>/<?php echo $link['url'] ?>"><?php echo $link['name'] ?></a></li>
		<?php endforeach ?>
	</ul>
	<div class="image">
		<img src="/img/event/logo/<?php echo $event['image'] ?>" width="200">
	</div>
	<div class="descr">
		<!-- <a class="expander" href="#">Прочитать описание ↓</a> -->
		<div class="content"><?php echo $event['descr'] ?></div>
	</div>
</div>