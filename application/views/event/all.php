<?php foreach ($events as $event): ?>
	<div class="all_events">
		<div class="item-event">
			<div class="image">
				<a href="/event/<?php echo $event['url'] ?>"><img src="/img/event/logo/<?php echo $event['image'] ?>" alt="<?php echo $event['name'] ?>" height="70"></a>
			</div>
			<div class="info">
				<h2 class="title"><a href="/event/<?php echo $event['url'] ?>"><?php echo $event['name'] ?></a></h2>
				<p class="time-place"><?php echo mdate("%d %M %Y в %H:%i", $event['date']) ?>, <?php echo $event['place'] ?></p>
				<p class="descr"><?php echo $event['short_descr'] ?></p>
			</div>
		</div>
	</div>
<?php endforeach ?>