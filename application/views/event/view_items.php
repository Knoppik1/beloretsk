<div class="list_items">
	<?php foreach ($items as $item): ?>

		<?php if ($type_fancybox == '1'): ?>

			<div class="item <?php echo $type_url ?> fancybox">
				<div class="image">
					<a title="<?php echo $item['descr'] ?>" rel="group-items" href="/img/event/<?php echo $event_url ?>/<?php echo $item['img'] ?>">
						<img src="/img/event/<?php echo $event_url ?>/mini/<?php echo $item['img'] ?>" width="240">
					</a>
				</div>
				<div class="descr">
					<?php echo $item['descr'] ?>
				</div>
			</div>

		<?php else: ?>

			<div class="item <?php echo $type_url ?>">
				<div class="image">
					<img src="/img/event/<?php echo $event_url ?>/mini/<?php echo $item['img'] ?>" width="240">
				</div>
				<div class="descr">
					<?php echo $item['descr'] ?>
				</div>
			</div>

		<?php endif ?>

	<?php endforeach ?>
</div>