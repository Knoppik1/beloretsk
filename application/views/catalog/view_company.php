<div class="company">

	<h1 class="name"><?php echo $company['name'] ?></h1>
	<div class="reviews">Просмотров: <?php echo $company['reviews'] ?></div>
	
	<div class="contacts">

		<?php if (isset($company['image'])): ?>
			<div class="logo_company">
				<img src="/img/logo/<?php echo $company['image'] ?>" width="150px">
			</div>
		<?php else: ?>
			<div class="logo_company">
				<img src="/img/no_image_2.png" width="70px">
			</div>
		<?php endif ?>

		<div class="field">
			<?php if ($addr = $company['address']): ?>
				<p class="addr"><?php echo $addr ?></p>
			<?php endif ?>

			<?php if ($phones !== FALSE): ?>
				<ul class="phones">
					<?php foreach ($phones as $p): ?>
						<li><?php echo $p['value'] ?></li>
					<?php endforeach ?>
				</ul>
			<?php endif ?>

			<?php if ($email = $company['email']): ?>
				<p class="email"><?php echo safe_mailto($email) ?></p>
			<?php endif ?>

			<?php if ($site = $company['site']): ?>
				<p class="site"><?php echo anchor($site, $site, array('target' => '_blank')) ?></p>
			<?php endif ?>

			<?php if ($social = $company['social']): ?>
				<p class="social"><?php echo auto_link($social, 'url', TRUE) ?></p>
			<?php endif ?>
		</div>

	</div>

	<p class="descr"><?php echo nl2br($company['descr']) ?></p>

</div>