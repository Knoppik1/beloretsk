<?php if ($message !== FALSE): ?>
	<div class="i_mess">
		<?php echo $message ?>
	</div>
<?php endif ?>

<?php if ($success !== FALSE): ?>
	<div class="i_succ">
		<?php echo $success ?>
	</div>
<?php endif ?>

<?php if (validation_errors() != FALSE): ?>
	<div class="i_err">
		<ul>
			<?php echo validation_errors() ?>
		</ul>
	</div>
<?php endif ?>

<?php if ($acts != FALSE): ?>
	<?php $as = ''; ?>
	<?php foreach ($acts as $a): ?>
		<?php $as .= '["' . $a['name'] . '",' . $a['activity_id'] . '],'; ?>
	<?php endforeach ?>
<?php endif ?>

<script type="text/javascript">
	$(function() {
		$("#activity").autocomplete({
			data: [<?php echo $as ?>],
			minChars: 1,
			useDelimiter: false,
			selectFirst: true,
			autoFill: false,
			mustMatch: true,
			onItemSelect: function(item) {
				var text = item.value;
				$('#activity_id').val(item.data);
			}
		});
	});
</script>

<form method="post" action="/catalog/add">

	<div class="add_company">

		<div class="field">
			<label for="company_name" class="general">Название организации</label>
			<input type="text" id="company_name" name="company_name" value="<?php echo set_value('company_name') ?>">
		</div>

		<div class="field">
			<label for="address" class="general">Адрес</label>
			<input type="text" id="address" name="address" placeholder="улица, дом, корпус, строение, офис" value="<?php echo set_value('address') ?>">
		</div>

		<div class="field phones">

			<div class="subfield phone_1">
				<label for="phone_1" class="general">Основной телефон</label>
				<input type="text" id="phone_1" name="phones[]" value="<?php echo set_value('phones[]') ?>">
				<div class="buttons">
					<span class="add"></span>
					<span class="del"></span>
				</div>
			</div>

			<input type="hidden" class="counter" value="1">
		</div>

		<div class="field">
			<label for="activity" class="general">Вид деятельности</label>
			<input type="text" id="activity" name="activity" value="<?php echo set_value('activity') ?>">
			<input type="hidden" id="activity_id" name="activity_id" value="0">
		</div>

		<div class="field">
			<label for="site">Сайт</label>
			<input type="text" id="site" name="site" value="<?php echo set_value('site') ?>">
		</div>

		<div class="field">
			<label for="social">Страница в соцсети</label>
			<input type="text" id="social" name="social" placeholder="Facebook, Вконтакте, Twitter и др." value="<?php echo set_value('social') ?>">
		</div>

		<div class="field">
			<label for="email">Эл. почта организации</label>
			<input type="text" id="email" name="email" value="<?php echo set_value('email') ?>">
		</div>

		<div class="field">
			<label for="short_descr">Краткое описание</label>
			<input type="text" id="short_descr" name="short_descr" class="long" value="<?php echo set_value('short_descr') ?>">
		</div>

		<div class="field">
			<label for="descr">Описание</label>
			<textarea id="descr" name="descr"><?php echo set_value('descr') ?></textarea>
		</div>

		<div class="submit">
			<input type="submit" value="Добавить организацию">
		</div>

	</div>

</form>