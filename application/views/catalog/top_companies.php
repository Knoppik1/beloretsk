<ol class="top_rating" start="1">
	<?php for ($i = 0; $i < 50; $i++): ?>
		<li>
			<div class="name">
				<a href="/catalog/<?php echo $companies[$i]['company_id'] ?>"><?php echo $companies[$i]['company_name'] ?></a>
			</div>
			<div class="reviews">
				<?php echo $companies[$i]['reviews'] ?> просмотров
			</div>
		</li>
	<?php endfor; ?>
</ol>
<ol class="top_rating" start="51">
	<?php for ($i = 50; $i < 100; $i++): ?>
		<li>
			<div class="name">
				<a href="/catalog/<?php echo $companies[$i]['company_id'] ?>"><?php echo $companies[$i]['company_name'] ?></a>
			</div>
			<div class="reviews">
				<?php echo $companies[$i]['reviews'] ?> просмотров
			</div>
		</li>
	<?php endfor; ?>
</ol>