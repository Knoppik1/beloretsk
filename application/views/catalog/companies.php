<div class="companies">

	<?php if (isset($companies) && $companies !== FALSE): ?>

		<?php foreach ($companies as $i => $company): ?>

			<?php $class = ($i % 2 == 0) ? 'c_odd' : '' ?>

			<div class="c_item <?php echo $class ?>">

				<div class="photo">
					<a href="/catalog/<?php echo $company['company_id'] ?>">
						<?php if (isset($company['image'])): ?>
							<img src="/img/logo/<?php echo $company['image'] ?>" width="75">
						<?php else: ?>
							<img src="/img/no_image.png" width="75">
						<?php endif ?>
					</a>
				</div>

				<div class="info">
					<h3 class="name"><?php echo anchor('/catalog/'.$company['company_id'], $company['company_name']) ?></h3>

					<?php if (isset($company['address'])): ?>
						<p class="addr"><?php echo $company['address'] ?></p>
					<?php endif ?>

					<?php if (isset($company['short_descr'])): ?>
						<p class="short_descr"><?php echo $company['short_descr'] ?></p>
					<?php endif ?>

					<p class="activity"><?php echo anchor('/catalog/activity/'.$company['activity_id'], $company['activity_name']) ?></p>
				</div>

			</div>

		<?php endforeach ?>

	<?php else: ?>
		Не найдено организаций.
	<?php endif ?>

</div>