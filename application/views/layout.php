<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<meta name="keywords" content="<?php echo $meta_keywords ?>" />
	<meta name="description" content="<?php echo $meta_descr ?>" />

	<title><?php echo $title ?></title>
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="shortcut icon" href="/img/favicon.png">

	<script type="text/javascript" src="/js/jquery-2.0.3.min.js"></script>

	<script type="text/javascript" src="/js/jquery.autocomplete.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/css/jquery.autocomplete.css">

	<script type="text/javascript" src="/js/fancybox/source/jquery.fancybox.pack.js"></script>
	<link rel="stylesheet" href="/js/fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />

	<script type="text/javascript" src="/js/common.js"></script>

	<meta name='yandex-verification' content='7a48afb2a0ee993e' />

</head>
<body>

	<div id="wrapper">

		<!-- {elapsed_time} / {memory_usage} -->

		<div id="head">

			<!-- Logo -->
			<a class="logo" href="<?php echo site_url() ?>"></a>

			<!-- Navigation -->
			<ul class="nav">
				<?php foreach ($nav as $url => $name): ?>
					<?php $class = ($this->uri->segment(1) == $url) ? 'active' : '' ?>
					<li><?php echo anchor(site_url($url), $name, array('class' => $class)) ?></li>
				<?php endforeach ?>
			</ul>

		</div>

		<ul class="top_links">
			<li><a href="/catalog/activity/582">Кафе</li>
			<li><a href="/catalog/activity/87">Банки</li>
			<li><a href="/catalog/activity/177">Кредиты</li>
			<li><a href="/catalog/activity/1489">Такси</li>
			<li><a href="/catalog/activity/1601">Такси-межгород</li>
			<li><a href="/catalog/activity/354">Аптеки</a></li>
			<li><a href="/catalog/top">Топ-100</a></li>
		</ul>

		<div id="header">

			<!-- Search -->
			<div class="search">
				<form action="/catalog/search/" method="post">
					<?php $s_value = (isset($s_query) && $s_query !== FALSE) ? $s_query : '' ?>
					<input type="text" name="query" maxlength="100" class="query" value="<?php echo $s_value ?>">
					<input type="submit" class="submit" value="Найти">
				</form>
			</div>

			<!-- Button -->
			<a class="btn_add_company" href="/catalog/add"></a>

		</div>

		<!-- Breadcrumbs -->
		<div id="breadcrumbs">

			<?php if (empty($breadcrumbs)): ?>
				Все организации в Белорецке, <span class="quantity"><?php echo $quantity_companies ?></span>

			<?php else: ?>

				<?php echo anchor(site_url(), 'Все организации в Белорецке', array('class' => 'first_link')) ?>

				<?php if (array_key_exists('pages', $breadcrumbs)): ?>
					<?php foreach ($breadcrumbs['pages'] as $url => $name): ?>
						<span class="b_arrow"></span>
						<?php echo anchor(site_url($url), $name) ?>
					<?php endforeach ?>
				<?php else: ?>
					<?php foreach ($breadcrumbs as $b): ?>
						<span class="b_arrow"></span>
						<?php echo anchor('/catalog/activity/'.$b['activity_id'], $b['name']); ?>
					<?php endforeach ?>
				<?php endif ?>

			<?php endif ?>

		</div>

		<div id="page">

			<div id="content">
				<?php echo $content ?>
			</div>

			<div id="sidebar">
				
				<?php if (isset($activities) && $activities !== FALSE): ?>
					<!-- Activities -->
					<ul class="activities">
						<?php foreach ($activities as $act): ?>
							<?php $class = ($act['activity_id'] == $this->uri->segment(3)) ? 'active' : '' ?>
							<li><?php echo anchor('/catalog/activity/'.$act['activity_id'], $act['name'], array('class' => $class)) ?></li>
						<?php endforeach ?>
					</ul>
				<?php endif ?>

				<?php if (isset($similar) && $similar !== FALSE): ?>
					<!-- Similar -->
					<div class="similar">
						Похожие организации:
						<?php foreach ($similar as $s): ?>
							<div class="s_item">

								<h4 class="name"><?php echo anchor('/catalog/'.$s['company_id'], $s['name']) ?></h4>

								<?php if (isset($s['address'])): ?>
									<p class="addr"><?php echo $s['address'] ?></p>
								<?php endif ?>

							</div>
						<?php endforeach ?>
					</div>
				<?php endif ?>
				
				<?php if (isset($random) && $random !== FALSE): ?>
					<!-- Random -->
					<div class="random">
						Случайные организации:
						<?php foreach ($random as $r): ?>
							<div class="r_item">
								<h4 class="name"><?php echo anchor('/catalog/'.$r['company_id'], $r['name']) ?></h4>
							</div>
						<?php endforeach ?>
					</div>
				<?php endif ?>
				
				<?php if (isset($links) && $links !== FALSE): ?>
					<!-- Links -->
					<ul class="links">
						<?php foreach ($links as $url => $name): ?>
							<li><?php echo anchor(site_url($url), $name) ?></li>
						<?php endforeach ?>
					</ul>
				<?php endif ?>

				<?php if (isset($blocks) && $blocks !== FALSE): ?>
					<!-- Blocks -->
					<div class="blocks">
						<?php foreach ($blocks as $title => $source): ?>
							<div class="d_item">
								<h4 class="title"><?php echo $title ?></h4>
								<div class="source"><?php echo $source ?></div>
							</div>
						<?php endforeach ?>
					</div>
				<?php endif ?>

			</div>

		</div>

		<?php if (isset($pagination) && $pagination !== FALSE): ?>
			<div id="pagination">
				<?php echo $this->pagination->create_links(); ?>
			</div>
		<?php endif ?>

		<div id="footer">

			<div class="f_menu">
				<a href="/catalog/about">О нас</a>
				<a href="http://vk.com/beloretsk_org">Мы ВКонтакте</a>
				<a href="http://vk.com/topic-59672590_28761901">Помочь каталогу</a>
				<a class="wtf" href="http://1762.рф/" title="18 июля 1762 года официальный день рождения Белорецкого завода и начало основания города Белорецк">1762.рф</a>
				<a href="/catalog/add">Добавить организацию</a>
			</div>

			<div class="f_about">
				Beloretsk.org — каталог организаций г.Белорецка
			</div>

		</div>
		
	</div>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
	(function (d, w, c) {
	    (w[c] = w[c] || []).push(function() {
	        try {
	            w.yaCounter19070746 = new Ya.Metrika({id:19070746,
	                    webvisor:true,
	                    clickmap:true,
	                    trackLinks:true,
	                    accurateTrackBounce:true});
	        } catch(e) { }
	    });

	    var n = d.getElementsByTagName("script")[0],
	        s = d.createElement("script"),
	        f = function () { n.parentNode.insertBefore(s, n); };
	    s.type = "text/javascript";
	    s.async = true;
	    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

	    if (w.opera == "[object Opera]") {
	        d.addEventListener("DOMContentLoaded", f, false);
	    } else { f(); }
	})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="//mc.yandex.ru/watch/19070746" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->

</body>
</html>