<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $title ?></title>
	<script type="text/javascript" src="/js/jquery-2.0.3.min.js"></script>
	<script type="text/javascript" src="/js/simple-expand.min.js"></script>
	<script type="text/javascript" src="/js/fancybox/source/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="/js/common-event.js"></script>
	<link rel="stylesheet" href="/css/event.css">
	<link rel="stylesheet" href="/js/fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />
	<link rel="shortcut icon" href="/img/favicon.png">
	<meta name='yandex-verification' content='7a48afb2a0ee993e' />
</head>
<body>
	<div id="wrapper">
		<div id="head">
			<a class="logo" href="<?php echo site_url() ?>"></a>

			<ul class="nav">
				<li><a href="/catalog">Организации</a></li>
				<li><a href="/event" class="active">События</a></li>
			</ul>

		</div>
		<div id="page">
			<h1 class="event_name"><?php echo $event_name ?></h1>
			<?php echo $content ?>
		</div>
		<div id="footer">
			Акции, анонсы, конкурсы, события в Белорецке <br />
			По вопросам сотрудничества: <?php echo safe_mailto('mail@beloretsk.org?subject=Events', 'mail@beloretsk.org') ?>
		</div>
	</div>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
	(function (d, w, c) {
	    (w[c] = w[c] || []).push(function() {
	        try {
	            w.yaCounter19070746 = new Ya.Metrika({id:19070746,
	                    webvisor:true,
	                    clickmap:true,
	                    trackLinks:true,
	                    accurateTrackBounce:true});
	        } catch(e) { }
	    });

	    var n = d.getElementsByTagName("script")[0],
	        s = d.createElement("script"),
	        f = function () { n.parentNode.insertBefore(s, n); };
	    s.type = "text/javascript";
	    s.async = true;
	    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

	    if (w.opera == "[object Opera]") {
	        d.addEventListener("DOMContentLoaded", f, false);
	    } else { f(); }
	})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="//mc.yandex.ru/watch/19070746" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
</body>
</html>