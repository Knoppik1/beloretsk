<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

// Default
$route['default_controller'] = "catalog";
$route['404_override'] = '';

// Pagination routes
$route['catalog/page/(:num)'] = 'catalog/index/$1';
$route['catalog/activity/(:num)/page/(:num)'] = 'catalog/view_activity/$1/$2';

// Pagination default page
$route['catalog/page'] = 'catalog/index/1';
$route['catalog/activity/(:num)/page'] = 'catalog/view_activity/$1/1';

// Company
$route['catalog/(:num)'] = 'catalog/view_company/$1';

// Activity
$route['catalog/activity/(:num)'] = 'catalog/view_activity/$1';

// Add company
$route['catalog/add'] = 'catalog/add_company';
$route['catalog/add/(:any)'] = 'catalog/add_company/$1';

$route['event/(:any)'] = 'event/view/$1';
$route['event/(:any)/(:any)'] = 'event/view/$1/$2';


/* End of file routes.php */
/* Location: ./application/config/routes.php */