<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
	'add_company' => array(
		array(
			'field' => 'company_name',
			'label' => 'Название организации',
			'rules' => 'required|trim|max_length[64]|min_length[2]|htmlspecialchars'
			),
		array(
			'field' => 'address',
			'label' => 'Адрес',
			'rules' => 'trim|max_length[255]|htmlspecialchars'
			),
		
		array(
			'field' => 'phones[]',
			'label' => 'Телефон',
			'rules' => 'trim|max_length[64]|htmlspecialchars'
			),
		array(
			'field' => 'activity_id',
			'label' => 'Вид деятельности',
			'rules' => 'required|trim|greater_than[0]'
			),
		array(
			'field' => 'site',
			'label' => 'Сайт',
			'rules' => 'trim|max_length[255]|min_length[3]|prep_url'
			),
		array(
			'field' => 'social',
			'label' => 'Страница в соцсети',
			'rules' => 'trim|max_length[255]|min_length[3]|prep_url'
			),
		array(
			'field' => 'email',
			'label' => 'Эл. почта организации',
			'rules' => 'trim|max_length[255]|min_length[3]|valid_email'
			),
		array(
			'field' => 'short_descr',
			'label' => 'Краткое описание',
			'rules' => 'trim|max_length[64]|min_length[7]|htmlspecialchars'
			),
		array(
			'field' => 'descr',
			'label' => 'Описание',
			'rules' => 'trim|max_length[3000]|min_length[3]|prep_for_form|xss_clean'
			),
		),
	);