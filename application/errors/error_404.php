<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>404</title>
</head>
<body>
	<style type="text/css">
		@import '/css/reset.css';
		body {
			font: 13px/1.4 Arial, sans-serif;
			margin: 100px;
		}
		h1 {
			font-size: 36px;
			margin: 0 0 10px 0;
			font-weight: bold;
		}
		.rabbit {
			text-align: center;
			height: 300px;
		}
		#container {
			width:300x;
		    height:420px;
		    margin:0 auto;
		    overflow:visible;
		    position:absolute;
		    left:50%;
		    top:50%;
		    margin-left:-150px;
		    margin-top:-210px;
		    text-align: center;
		}
	</style>
	<div id="container">
		<p class="rabbit">
			<img src="/img/404.jpg" alt="Кролик" height="300" >
		</p>
		<h1>О, нет!</h1>
		<p>
			Что-то пошло не так, и эта ссылка не работает.<br />
			Но вы всегда можете <a href="/">вернуться на главную страницу</a>.
		</p>
	</div>
</body>
</html>