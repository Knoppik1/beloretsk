<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catalog extends CI_Controller {

	// global
	var $data = array();
	var $activities = array();
	var $p_config = array();

	function __construct()
	{
		parent::__construct();

		// load
		$this->load->model('catalog_model', 'catalog');

		// Sessions
		$this->load->library('session');

		if ($this->session->userdata('viewed_companies') == FALSE) {
			$this->session->set_userdata(array('viewed_companies' => array()));
		}

		// print_r($this->session->userdata('viewed_companies'));

		// global data
		$this->activities = $this->catalog->get_activities();

		// layout2
		$this->layout2->setTitle('Организации');
		$this->layout2->setNav(array('catalog' => 'Организации', 'event' => 'События'));
		$this->layout2->setCount($this->catalog->count_companies());

		// pagination config
		$this->p_config = array(
			'use_page_numbers' 	=> TRUE,
			'first_link' 		=> 'Первая',
			'last_link' 		=> 'Последняя',
			'next_link'			=> 'Следующая &rarr;',
			'prev_link' 		=> '&larr; Предыдущая',
			'num_links' 		=> 4,
			'per_page'			=> 10,
			'first_url'			=> '1'
		);

		// profilier
		// $this->output->enable_profiler(TRUE);

	}

	// Главная
	public function index($page = 1)
	{
		if ($this->uri->segment(1) == '') {
			redirect('/catalog/');
		}

		// Pagination
		$start = ($page - 1) * $this->p_config['per_page'];

		$this->p_config['base_url'] = site_url('catalog/page');
		$this->p_config['total_rows'] = count($this->catalog->get_companies());

		$this->pagination->initialize($this->p_config);

		// Данные страницы
		$this->data = array(
			'activities'	=> $this->_get_activities(0),
			'companies'		=> $this->catalog->get_companies($this->p_config['per_page'], $start, FALSE, 'company_id'),
			'breadcrumbs'	=> $this->_get_breadcrumbs(0),
			'pagination'	=> $this->pagination->create_links(),
		);

		$this->layout2->view('catalog/welcome', $this->data);
	}

	// Просмотр деятельности
	public function view_activity($activity_id, $page = 1)
	{
		// Загружаем деятельность
		$activity = $this->_get_activity($activity_id);

		// Если деятельность не найдена
		if ($activity == FALSE) show_404();

		// Заголовок страницы
		$this->layout2->setTitle($activity['name']);
		$this->layout2->setKeywords($activity['name'] . ', белорецк');
		$this->layout2->setDescr($activity['name'] . ' в Белорецке');

		// Деятельности для вывода списка компаний
		$activities_id = array($activity_id);

		// Потомки деятельности
		if (!$activities = $this->_get_activities($activity_id)) 
		{
			$parent = $this->_get_activity_parent($activity_id);
			$activities = $this->_get_activities($parent);
		}

		// Добавляем потомков в список вывода компаний 
		if ($childrens = $this->_get_childrens_activity($activity_id)) {
			foreach ($childrens as $ch) {
				$activities_id[] = $ch;
			}
		}

		// Pagination
		$start = ($page - 1) * $this->p_config['per_page'];

		$this->p_config['base_url'] = site_url('catalog/activity/'.$activity_id.'/page/');
		$this->p_config['total_rows'] = count($this->catalog->get_companies(FALSE, 0, $activities_id));
		$this->p_config['uri_segment'] = 5;

		$this->pagination->initialize($this->p_config);

		// Данные для отображения
		$this->data = array(
			'breadcrumbs'	=> $this->_get_breadcrumbs($activity_id),
			'activities' 	=> $activities,
			'companies' 	=> $this->catalog->get_companies($this->p_config['per_page'], $start, $activities_id, 'company_id', 'desc'),
			'pagination'	=> $this->pagination->create_links(),
		);

		$this->layout2->view('catalog/view_activity', $this->data);
	}

	/* --------------------
	        TOP 100
	--------------------- */

	function top()
	{
		$this->layout2->setTitle('Топ-100 компаний');
		$this->data = array(
			'companies' => $this->catalog->get_companies(100, 0, 0, 'reviews', 'desc'),
		);
		$this->layout2->view('catalog/top_companies', $this->data);
	}

	/* --------------------
	   Просмотр компании 
	--------------------- */

	public function view_company($company_id)
	{
		// Если компания не найдена
		if ($this->catalog->get_company($company_id) == FALSE) show_404();

		// Просмотреннны компании
		$viewed_companies = $this->session->userdata('viewed_companies');

		// Если компания еще не просматривалась
		if (in_array($company_id, $viewed_companies) == FALSE) 
		{
			$viewed_companies[] = $company_id;
			$this->session->unset_userdata('viewed_companies');
			// Добавляем текущую компанию к просмотренным
			$this->session->set_userdata(array('viewed_companies' => $viewed_companies));
			// Обновляем счетчик просмотров компании
			$this->catalog->update_company_reviews($company_id);
		}

		// $this->session->sess_destroy();

		// Загружаем компанию
		$company = $this->catalog->get_company($company_id);

		// Заголовок страницы
		$this->layout2->setTitle($company['name']);
		$this->layout2->setDescr($company['short_descr'] . ' в Белорецке');
		$this->layout2->setKeywords($company['name'] . ', белорецк');

		// Деятельность компании
		$activity_id = $company['activity_id'];

		// Случайные компании
		if (!$similar = $this->catalog->get_similar_companies($activity_id, $company_id)) {
			$random = $this->catalog->get_random_companies(10);
		} 
		else {
			$random = FALSE;
		}

		// Данные отображения
		$this->data = array(
			'breadcrumbs' 	=> $this->_get_breadcrumbs($activity_id),
			'company'		=> $company,
			'phones'		=> $this->catalog->get_company_phones($company_id),
			'similar'		=> $similar,
			'random'		=> $random,
		);

		$this->layout2->view('catalog/view_company', $this->data);
	}

	// Добавление компании
	public function add_company($success = FALSE)
	{
		// Cообщение об ошибке для вида деятельности
		$this->form_validation->set_message('greater_than', 'Поле "%s" обязательно для заполнения.');

		// Разделители ошибок
		$this->form_validation->set_error_delimiters('<li>', '</li>');

		$succ = FALSE;
		$mess = FALSE;

		// Сообщения
		if ($success == 'success')
			$succ = "Организация успешно добавлена, после проверки она появится на сайте";
		else
			$mess = "Перед добавлением на сайт каждая заявка проверяется на соответствие правилам";

		// Если форма прошла валидацию
		if ($this->form_validation->run('add_company') == TRUE) 
		{
			$post = $this->input->post();

			// Данные компании
			$company = array(

					// require
					'name' 			=> $post['company_name'],
					'activity_id' 	=> $post['activity_id'],
					'address'		=> $this->_check_empty($post['address']),

					// secondary
					'short_descr' 	=> $this->_check_empty($post['short_descr']),
					'descr' 		=> $this->_check_empty($post['descr']),
					'site' 			=> $this->_check_empty($post['site']),
					'social' 		=> $this->_check_empty($post['social']),
					'email' 		=> $this->_check_empty($post['email']),
					'approved'		=> '0',
				);

			// Добавляем компанию
			$this->catalog->add_company($company);

			// ID новой компании
			$company_id = $this->db->insert_id();

			// Защита от добавления больше 5 номеров
			$ph = array_chunk($post['phones'], 5);

			$phones = array();

			// Номера телефонов
			foreach ($ph[0] as $p) 
			{
				if (!empty($p)) {
					$phones[] = array(
						'company_id'	=> $company_id, 
						'value'			=> $p
					);
				}
			}

			// Добавляем телефоны
			if (sizeof($phones) != 0) {
				$this->catalog->add_phones($phones);
			}

			// Редирект
			redirect('/catalog/add/success');
		}

		// Заголовок страницы
		$this->layout2->setTitle('Добавление');

		$info = "<ul>
					<li>Логотип и фотографии высылайте на ".safe_mailto('mail@beloretsk.org')."</li>
					<li>Вы можете прислать информацию о уже имеющейся в каталоге компании по адресу ".safe_mailto('mail@beloretsk.org')."</li>
					<li>Не подавайте одну и ту же заявку на добавление повторно</li>
					<li>У вас больше одного офиса? — добавьте компанию несколько раз</li>
					<li>Не пишите лишнюю информацию в названии компании, для этого есть поле описания</li>
					<li>В кратком описании должна быть краткая и лаконичная фраза, характеризующая вас</li>
					<li>В описании напишите подробно об ассортименте, преимуществах, часах работы и др.</li>
					<li>В поле деятельности введите часть слова и выберите подходящую рубрику из списка</li>
					<li>Эл. адрес нужен для обратной связи с клиентами (адреса не продаются спамерам, а на странице компании зашифровываются от ботов)</li>
				</ul>";

		// Данные для страницы
		$this->data = array(
			'acts' 			=> $this->_get_activities(),
			// 'links'			=> array('catalog/about' => 'О каталоге'),
			'breadcrumbs' 	=> array('pages' => array('catalog/add' => 'Добавление организации')),
			'blocks'		=> array('Информация' => $info),
			'success'		=> $succ,
			'message'		=> $mess,
		);

		$this->layout2->view('catalog/add_company', $this->data);

		
	}

	// Поиск компаний
	function search()
	{
		$query = $this->input->post('query');

		if ($query != FALSE)
			$companies = $this->catalog->search_companies($query);
		else $companies = FALSE;

		$this->layout2->setTitle('Поиск');

		$this->data = array(
			'companies' => $companies,
			's_query' 	=> $query,
		);

		$this->layout2->view('catalog/search', $this->data);
	}

	// О сайте
	function about()
	{
		$this->layout2->setTitle('О нас');

		$this->data = array(
			'random' => $this->catalog->get_random_companies(20),
			'breadcrumbs' => array('pages' => array('catalog/about' => 'О каталоге')),
		);

		$this->layout2->view('catalog/about', $this->data);
	}

	// --------------------------------------------------
	// Внутренние функции
	// --------------------------------------------------

	// Замена пустых значений в форме добавления на NULL
	function _check_empty($s)
	{
		if (empty($s))
			$s = 'NULL';
		else return $s;
	}

	// Генерация хлебных крошек
	function _get_breadcrumbs($activity_id)
	{
		$parent = ($activity_id !== 0) ? $this->_get_activity($activity_id) : FALSE;

		$breadcrumbs = array();

		while ($parent !== FALSE) 
		{	
			$activity_id = $parent['parent_id'];

			$breadcrumbs[] = array(
				'activity_id' => $parent['activity_id'], 
				'name' => $parent['name'], 
				'parent_id' => $parent['parent_id']
			);

			$parent = $this->_get_activity($activity_id);
		}

		// Реверс с сохранением ключей массива
		return array_reverse($breadcrumbs, TRUE);
	}

	// Возвращает деятельность
	function _get_activity($activity_id)
	{
		foreach ($this->activities as $act) {
			if ($act['activity_id'] == $activity_id) {
				$activity = $act;
			}
		}

		if (isset($activity)) {
			return $activity;
		} else {
			return FALSE;
		}
	}

	// Возвращает список потомков деятельности
	function _get_activities($parent_id = FALSE)
	{
		// Родитель указан, возвращаем массив $a[parent]
		if (is_numeric($parent_id)) 
		{
			foreach ($this->activities as $act) 
			{
				$result[$act['parent_id']][] = $act;

				// $activities_id = $this->_get_childrens_activity($act['activity_id']);
				// $result[$act['parent_id']][$num]['quantity'] = $this->catalog->count_companies($activities_id);
			}

			if (isset($result[$parent_id])) 
			{
				return $result[$parent_id];
			} 
			else 
			{
				return FALSE;
			}
		} 
		// Если не указан - возвращаем обычные массив
		else 
		{
			return $this->activities;
		}
	}

	// Возвращает родителя деятельности
	function _get_activity_parent($activity_id)
	{
		foreach ($this->activities as $act) {
			if ($act['activity_id'] == $activity_id) {
				$activity = $act['parent_id'];
			}
		}

		if (isset($activity)) {
			return $activity;
		} else {
			return FALSE;
		}
	}

	// Рекурсивная ф-ия, возвращающая список всех потомков деятельности
	function _get_childrens_activity($activity_id)
	{
		if (is_array($a = $this->_get_activities($activity_id))) 
		{
			foreach ($a as $k) 
			{
				$r[] = $k['activity_id'];

				// Ищем вглубь
				if ($b = $this->_get_childrens_activity($k['activity_id'])) 
				{
					foreach ($b as $m) {
						$r[] = $m;
					}
				}
			}
		}
		else return FALSE;

		return $r;
	}
}