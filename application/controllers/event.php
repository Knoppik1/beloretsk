<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('events');
		$this->layout->setLayout('layout_event');
		$this->load->helper('date');
	}

	// главная
	function index()
	{
		$this->all();
	}

	// список всех событий
	function all()
	{
		
		$this->layout->setTitle('События / Белорецк');

		$data = array(
			'events' => $this->events->get_all(),
			'event_name' => 'События в Белорецке',
		);

		$this->layout->view('event/all', $data);
	}

	// просмотр события
	function view($url, $type_url = FALSE)
	{
		$event = $this->events->get($url);

		// событие
		if (!$type_url) 
		{
			if (!$event) show_404();

			$this->layout->setTitle($event['name'] . ' / Белорецк');

			$data = array(
				'event' => $event,
				'event_name' => $event['name'],
				'links' => $this->events->get_types($event['event_id']),
			);

			$this->layout->view('event/view', $data);
		} 
		// материалы события
		else 
		{
			$type = $this->events->get_type($type_url);

			if (!$event || !$type) show_404();

			$this->layout->setTitle($type['name'] . ' / ' . $event['name']);

			$data = array(
				'items' => $this->events->get_items($event['event_id'], $type['type_id']),
				'event_name' => '<a href="/event/'.$event['url'].'/"'.'>'.$event['name'].'</a>'.' &rarr; '.$type['name'],
				'event_url' => $event['url'],
				'type_url' => $type_url,
				'type_fancybox' => $type['fancybox'],
			);

			$this->layout->view('event/view_items', $data);
		}
	}

}

/* End of file event.php */
/* Location: ./application/controllers/event.php */