<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catalog_model extends CI_Model 
{
    // Список новых организаций
    function get_companies($limit = FALSE, $start = 0, $activities_id = FALSE, $order = FALSE, $sort = 'desc')
    {
        $this->db->select('c.company_id, c.short_descr, c.address, c.reviews, c.name AS company_name, c.image, a.activity_id, a.name AS activity_name')
                 ->where('approved', '1')
                 ->join('activities AS a', 'c.activity_id = a.activity_id');

        // Лимит
        if ($limit !== FALSE) {
            $this->db->limit($limit, $start);
        }

        // Сортировка
        if ($order) {
            $this->db->order_by($order, $sort);
        } else {
            $this->db->order_by('company_name', 'asc');
        }

        // Рубрика
        if ($activities_id) 
        {
            $this->db->where_in('a.activity_id', $activities_id);
        }

        $query = $this->db->get('companies AS c');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return FALSE;
    }

    // Обновление счетчика просмотров компании
    function update_company_reviews($company_id)
    {
        $this->db->query("UPDATE companies SET reviews = reviews + 1 WHERE company_id = $company_id");
    }

    // Инфо о компании
    function get_company($company_id)
    {
        $this->db->where(
            array(
                'approved' => '1',
                'company_id' => $company_id
            ));

        $query = $this->db->get('companies', 1, 0);

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return FALSE;
    }

    // Похожие компании
    function get_similar_companies($activity_id, $company_id)
    {
        $this->db->where(
            array(
                'activity_id'   => $activity_id,
                'company_id !=' => $company_id,
                'approved'      => '1',
            )
        );

        $this->db->order_by('name', 'asc');

        $query = $this->db->get('companies');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return FALSE;
    }

    // Случайные компании
    function get_random_companies($limit = 5)
    {
        $q = "SELECT `company_id`, `name` FROM `companies` AS `r1`
              JOIN (SELECT (RAND() * (SELECT MAX(`company_id`) FROM `companies`)) AS `id`) AS `r2` 
              WHERE `r1`.`company_id` >= `r2`.`id` AND `approved` = '1'
              ORDER BY `r1`.`company_id` ASC 
              LIMIT $limit";

        $query = $this->db->query($q);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return FALSE;

    }

    // Телефоны компании
    function get_company_phones($company_id)
    {
        $query = $this->db->get_where('phones', "company_id = $company_id");

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return FALSE;
    }

    // Возращает название компании
    function get_company_name($company_id)
    {
        $query = $this->db->get_where('companies', "company_id = $company_id");

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result['name'];
        } else return FALSE;
    }

    // Список адресов компании
    function get_company_addresses($company_id)
    {
        $query = $this->db->get_where('addresses', "company_id = $company_id");

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return FALSE;
    }


    // Список всех деятельностей
    function get_activities()
    {
        $this->db->order_by('name', 'asc');

        $query = $this->db->get_where('activities', array('visible' => '1'));

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return FALSE;
    }

    // Поиск компаний
    function search_companies($query)
    {
        // Поиск:
        $this->db->select('c.company_id, c.name AS company_name, c.short_descr, c.image, c.address, a.name AS activity_name, a.activity_id');
        $this->db->from('companies AS c');
        $this->db->join('activities a', 'c.activity_id = a.activity_id');

        // В именах
        $this->db->like('c.name', $query); 
        // В кратком описании
        $this->db->or_like('c.short_descr', $query);
        // В подробном описании
        $this->db->or_like('c.descr', $query);

        // Только одобренные
        $this->db->where('c.approved', '1');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return FALSE;
    }

    // Добавление компании
    function add_company($data)
    {
        $this->db->insert('companies', $data); 
    }

    // Добавление телефонов компании
    function add_phones($phones)
    {
        $this->db->insert_batch('phones', $phones); 
    }

    // Проверка названия компании на существование
    function check_company_name($name)
    {
        $this->db->like('name', $name, 'none'); 

        $query = $this->db->get('companies');

        if ($query->num_rows() > 0) 
        {
            return TRUE;
        } else return FALSE;
    }

    // Подсчет компаний
    function count_companies($activities_id = FALSE)
    {
        // $this->db->cache_on();
        
        // Подсчет всех компаний
        if ($activities_id === FALSE) 
        {
            return $this->db->count_all('companies');
        }

        // Подсчет из подрубрик
        if (is_array($activities_id)) 
        {
            $this->db->select('company_id');
            $this->db->where_in('activity_id', $activities_id);
            return $this->db->count_all_results();
        }
    }
}