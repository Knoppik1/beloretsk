<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Events extends CI_Model 
{
	// список событий
	function get_all()
	{
		$q = $this->db->get('events');
		return $q->result_array();
	}

	// событие
	function get($url)
	{
		$q = $this->db->get_where('events', array('url' => $url));

		if ($q->num_rows() > 0) {
			return $q->row_array();
		} else {
			return FALSE;
		}
	}

	// типы материалов события
	function get_types($event_id)
	{
		$q = $this->db->get_where('events_types', array('event_id' => $event_id));
		
		if ($q->num_rows() > 0) {
			return $q->result_array();
		} else {
			return FALSE;
		}
	}

	// элементы 
	function get_items($event_id, $type_id)
	{
		$this->db
			->order_by('item_id', 'desc')
			->where(array('event_id' => $event_id, 'type_id' => $type_id));

		$q = $this->db->get('events_items');
		return $q->result_array();
	}

	// инфо о типе материала
	function get_type($name)
	{
		$q = $this->db->get_where('events_types', array('url' => $name));

		if ($q->num_rows() > 0) {
			return $q->row_array();
		} else {
			return FALSE;
		}
	}
}

/* End of file events.php */
/* Location: ./application/models/events.php */