$(document).ready(function() 
{
	// add
	$(document).on('click', '.add', function() 
	{
		var num = parseInt($('.counter').val())+1;
		var maxsize = 5;

		$('.counter').val(num);
		if (num > maxsize) {return false};

		$('<div class="phone_' + num + '"><label for="phone_' + num + '">Дополнительные</label><input type="text" name="phones[]" id="phone_' + num + '"><div class="buttons"><span class="add"></span> <span class="del" rel="' + num + '"></div></div>').appendTo('.phones');
	});

	// delete
	$(document).on('click', '.del', function() 
	{
		if (num == 1) {
			return false;
		} else {
			var num = $(this).attr('rel');
			$('.counter').val(num-1);
			$('.phone_'+num).remove();
		};
	});

	// fancybox
	$('.photos a').fancybox();
});