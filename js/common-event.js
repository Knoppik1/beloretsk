$(document).ready(function() 
{
	$(".event").mouseover(function() {$(this).addClass("over");});
	$(".event").mouseout(function() {$(this).removeClass("over");});

	$('.expander').simpleexpand();

	$('.list_items .fancybox .image a').fancybox({
		'transitionIn'  : 'fade',
		'transitionOut' : 'fade',
		'titlePosition' : 'over',
		'titleFormat'   : function(title, currentArray, currentIndex, currentOpts) {
			return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? '   ' + title : '') + '</span>';
		}
	});

});